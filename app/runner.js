requirejs.config({
    paths: {
        'jquery': '../libs/jquery',
        'jquery-private': '../libs/jquery-private'
    },
    map: {
        '*': { 'jquery': 'jquery-private' },
        'jquery-private': { 'jquery': 'jquery' }
    }
});
requirejs(['main']);
